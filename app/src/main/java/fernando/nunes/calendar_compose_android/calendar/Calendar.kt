@file:OptIn(ExperimentalMaterial3Api::class)

package fernando.nunes.calendar_compose_android.calendar

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fernando.nunes.calendar_compose_android.calendar.model.CalendarBlock
import fernando.nunes.calendar_compose_android.calendar.model.CalendarColors
import fernando.nunes.calendar_compose_android.calendar.model.StableDate
import fernando.nunes.calendar_compose_android.calendar.model.backgroundColor
import fernando.nunes.calendar_compose_android.calendar.model.backgroundSecondaryColor
import fernando.nunes.calendar_compose_android.calendar.model.primaryColor
import fernando.nunes.calendar_compose_android.calendar.model.textDarkColor
import fernando.nunes.calendar_compose_android.calendar.model.textDarkColor2
import fernando.nunes.calendar_compose_android.calendar.model.textLightColor
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.Month
import kotlin.random.Random



@Composable
fun Calendar(
    years: ImmutableList<Int>,
    includedNights: Long,
    checkIn: StableDate?,
    checkOut: StableDate?,
    onDateSelected: (StableDate) -> Unit,
    modifier: Modifier = Modifier,
    hasSameYearMonthAndDay: (Int, Int, Int) -> Boolean = { year, month, day -> true }
) {

    var blocks by remember {
        mutableStateOf<ImmutableList<CalendarBlock>>(persistentListOf())
    }

    LaunchedEffect(years) {
        blocks = CalendarBlock.buildCalendarBlocks(years)
    }

    CalendarContent(
        blocks = blocks,
        includedNights = includedNights,
        checkIn = checkIn,
        checkOut = checkOut,
        onDateChanged = onDateSelected,
        modifier = modifier,
        hasSameYearMonthAndDay = hasSameYearMonthAndDay,
    )
}

@Composable
private fun CalendarContent(
    blocks: ImmutableList<CalendarBlock>,
    includedNights: Long,
    checkIn: StableDate?,
    checkOut: StableDate?,
    onDateChanged: (StableDate) -> Unit,
    modifier: Modifier,
    hasSameYearMonthAndDay: (Int, Int, Int) -> Boolean
) {
    Scaffold(
        topBar = {
            Surface(
                shadowElevation = 8.dp
            ) {
                CalendarWeekDaysBlock()
            }
        },
        modifier = modifier
            .shadow(4.dp),
    ) { padding ->
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(padding)
        ) {
            items(
                items = blocks,
                key = {
                    when (it) {
                        is CalendarBlock.CalendarYearAndMonthBlock -> "year_month_block_${it.year}/${it.month}"
                        is CalendarBlock.CalendarDaysBlock -> "days_block_${it.year}/${it.month}"
                    }
                }
            ) {
                when (it) {
                    is CalendarBlock.CalendarYearAndMonthBlock -> {
                        CalendarYearAndMonthBlock(
                            year = it.year,
                            month = it.month
                        )
                    }

                    is CalendarBlock.CalendarDaysBlock -> {
                        CalendarDaysBlock(
                            year = it.year,
                            month = it.month,
                            calendarDaysContent = { day ->
                                val date = StableDate(it.year, it.month, day)
                                if (hasSameYearMonthAndDay(it.year, it.month, day)) {
                                    CalendarDay(
                                        day = day,
                                        date = date,
                                        includedNights = includedNights,
                                        checkIn = checkIn,
                                        checkOut = checkOut,
                                        onDateChanged = onDateChanged
                                    )
                                } else {
                                    val onClick: () -> Unit = remember(date) { { onDateChanged(date) } }
                                    CalendarDay(
                                        day = day,
                                        onClick = onClick
                                    )
                                }
                            }
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun CalendarYearAndMonthBlock(
    year: Int,
    month: Int
) {

    val monthString = Month.of(month).name.lowercase().replaceFirstChar { it.uppercaseChar() }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp)
            .background(
                color = backgroundSecondaryColor
            )
    ) {
        Text(
            text = "$monthString $year",
            style = MaterialTheme.typography.labelLarge,
            color = textDarkColor2,
            modifier = Modifier
                .align(Alignment.CenterStart)
                .padding(start = 16.dp)
        )
    }
}

@Composable
fun CalendarWeekDaysBlock() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(30.dp),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.CenterVertically
    ) {
        repeat(7) {
            Box(
                modifier = Modifier
                    .weight(1f),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = DayOfWeek.of(it + 1).name.first().toString(),
                    style = MaterialTheme.typography.labelMedium,
                )
            }
        }
    }
}

@Composable
private fun CalendarDaysBlock(
    year: Int,
    month: Int,
    calendarDaysContent: @Composable (Int) -> Unit
) {
    val daysInMonth = LocalDate.of(year, month, 1).lengthOfMonth()
    val firstDayOfMonth = LocalDate.of(year, month, 1).dayOfWeek.value % 7
    val weeksInMonth = (firstDayOfMonth + daysInMonth + 6) / 7

    Column(
        modifier = Modifier
            .background(color = backgroundColor)
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        repeat(weeksInMonth) { row ->
            Row {
                repeat(7) { cell ->
                    val index = (row * 7) + cell
                    Box(
                        modifier = Modifier
                            .weight(1f),
                        contentAlignment = Alignment.Center
                    ) {
                        if (index >= firstDayOfMonth && index < firstDayOfMonth + daysInMonth) {
                            val day = index - firstDayOfMonth + 1
                            calendarDaysContent(day)
                        } else {
                            Spacer(modifier = Modifier.padding(8.dp))
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CalendarDay(
    day: Int,
    date: StableDate,
    includedNights: Long,
    checkIn: StableDate?,
    checkOut: StableDate?,
    onDateChanged: (StableDate) -> Unit
) {

    val localDate = date.localDate

    val onClick: () -> Unit = remember(date) {
        {
            onDateChanged(date)
        }
    }

    val checkInDate = checkIn?.localDate
    val checkOutDate = checkOut?.localDate

    val dateRange: ClosedRange<LocalDate>? = when {
        checkInDate == null || checkOutDate == null -> null
        else -> checkInDate..checkOutDate
    }

    val isCheckInDate = checkInDate != null && checkInDate == localDate
    val isPredictedCheckOutDate = checkInDate != null &&
            checkOutDate == null &&
            localDate == checkInDate.plusDays(includedNights)
    val isCheckOutDate = checkOutDate != null && localDate == checkOutDate
    val isSelected = isCheckInDate || dateRange != null && dateRange.contains(localDate)
    val isIncludedNight = checkInDate != null && localDate in checkInDate..checkInDate.plusDays(includedNights).minusDays(1)
    val isExtraNight = checkInDate != null && isSelected && localDate.isAfter(checkInDate.plusDays(includedNights - 1))


    CalendarDay(
        day = day,
        isCheckIn = isCheckInDate,
        isCheckOut = isCheckOutDate,
        isPredictedCheckOutDate = isPredictedCheckOutDate,
        isIncludedNight = isIncludedNight,
        isExtraNight = isExtraNight,
        isSelected = isSelected,
        onClick = onClick
    )
}

@Composable
fun CalendarDay(
    day: Int,
    isCheckIn: Boolean = false,
    isCheckOut: Boolean = false,
    isPredictedCheckOutDate: Boolean = false,
    isIncludedNight: Boolean = false,
    isExtraNight: Boolean = false,
    isSelected: Boolean = false,
    onClick: () -> Unit
) {

    val backgroundColor = when {
        isSelected -> primaryColor
        else -> Color.Transparent
    }

    val shape = RoundedCornerShape(
        topStart = if (isCheckIn) 15.dp else 0.dp,
        bottomStart = if (isCheckIn) 15.dp else 0.dp,
        topEnd = if (isCheckOut) 15.dp else 0.dp,
        bottomEnd = if (isCheckOut) 15.dp else 0.dp,
    )


    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(30.dp)
                .background(
                    color = backgroundColor,
                    shape = shape
                )
                .border(
                    width = if (!isSelected && (isIncludedNight || isPredictedCheckOutDate)) 1.dp else 0.dp,
                    color = if (!isSelected && (isIncludedNight || isPredictedCheckOutDate)) primaryColor else Color.Transparent,
                    shape = RectangleShape
                )
                .clip(shape)
                .clickable {
                    onClick()
                }
        ) {
            Text(
                text = day.toString(),
                style = MaterialTheme.typography.labelMedium.copy(
                    color = if (isSelected) textLightColor else textDarkColor,
                    fontSize = 14.sp
                ),
                maxLines = 1,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .align(Alignment.Center)

            )
        }

        if (isIncludedNight) {
            Icon(
                imageVector = Icons.Rounded.Check,
                contentDescription = "Included night on the day $day",
                tint = primaryColor,
                modifier = Modifier
                    .size(12.dp)
            )
        }

        if (!isCheckOut && !isIncludedNight && (!isSelected || isExtraNight)) {
            val priceTextColor = if (isSelected) primaryColor else textDarkColor

            Text(
                text = "199.99",
                style = MaterialTheme.typography.labelSmall.copy(
                    fontSize = 9.sp,
                    color = priceTextColor
                ),
                maxLines = 1,
                textAlign = TextAlign.Center,
            )
        }


    }
}

private fun getRandomColor(): Color {
    return Color(
        red = Random.nextInt(255),
        green = Random.nextInt(255),
        blue = Random.nextInt(255),
    )
}

