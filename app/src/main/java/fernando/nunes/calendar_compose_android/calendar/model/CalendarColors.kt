package fernando.nunes.calendar_compose_android.calendar.model

import androidx.compose.ui.graphics.Color

val primaryColor = Color(0xFFEB5F40)
val textDarkColor = Color(0xFF1C1C1C)
val textDarkColor2 = Color(0xFF595959)
val textLightColor = Color(0xFFFFFFFF)
val backgroundColor = Color(0xFFFFFFFF)
val backgroundSecondaryColor = Color(0xFFFAFAFA)
data class CalendarColors(
    val monthAndYearBackgroundColor: Color = backgroundSecondaryColor,
    val monthAndYearTextColor: Color = textDarkColor2,
    val daysBlockBackgroundColor: Color = backgroundColor,
    val unselectedDayTextColor: Color = textDarkColor,
    val unselectedPriceTextColor: Color = textDarkColor,
    val selectedDayTextColor: Color = textLightColor,
    val selectedPriceTextColor: Color = primaryColor,
    val selectedDayBackgroundColor: Color = primaryColor,
    val predictedDayBorderColor: Color = primaryColor,
)
