package fernando.nunes.calendar_compose_android.calendar.model

import androidx.compose.runtime.Stable
import java.time.LocalDate

@Stable
data class StableDate(
    val year: Int,
    val month: Int,
    val day: Int,
)  {

    val localDate: LocalDate
        get() = LocalDate.of(year, month, day)

    companion object {
        fun LocalDate.toStableDate(): StableDate {
            return StableDate(year, month.value, dayOfMonth)
        }
    }
}
