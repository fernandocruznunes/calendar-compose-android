package fernando.nunes.calendar_compose_android.calendar.model

import androidx.compose.runtime.Stable
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.toImmutableList

@Stable
sealed class CalendarBlock private constructor(
    open val year: Int,
    open val month: Int
) {

    data class CalendarYearAndMonthBlock(
        override val year: Int,
        override val month: Int,
    ): CalendarBlock(year, month)

    data class CalendarDaysBlock(
        override val year: Int,
        override val month: Int,
    ): CalendarBlock(year, month)

    companion object {

        fun buildCalendarBlocks(years: ImmutableList<Int>): ImmutableList<CalendarBlock> {
            val blocks: ArrayList<CalendarBlock> = arrayListOf()
            years.forEach { year ->
                repeat(12) { month ->
                    blocks.add(
                        CalendarYearAndMonthBlock(
                            year = year,
                            month = month + 1,
                        )
                    )

                    blocks.add(
                        CalendarDaysBlock(
                            year = year,
                            month = month + 1,
                        )
                    )
                }
            }

            return blocks.toImmutableList()
        }

    }
}
