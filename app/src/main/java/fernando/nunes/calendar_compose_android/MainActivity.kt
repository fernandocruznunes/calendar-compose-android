package fernando.nunes.calendar_compose_android

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import fernando.nunes.calendar_compose_android.calendar.Calendar
import fernando.nunes.calendar_compose_android.calendar.model.StableDate.Companion.toStableDate
import fernando.nunes.calendar_compose_android.ui.theme.CalendarComposeAndroidTheme
import kotlinx.collections.immutable.toImmutableList
import java.time.LocalDate
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalendarComposeAndroidTheme {
                var includedNights by remember {
                    mutableStateOf(getIncludedNights())
                }

                var checkInDate by remember {
                    mutableStateOf<LocalDate?>(null)
                }

                var checkOutDate by remember {
                    mutableStateOf<LocalDate?>(null)
                }


                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column {
                        Text(text = "Included Nights: $includedNights")
                        Calendar(
                            years = (2020..2024).toImmutableList(),
                            includedNights = includedNights,
                            checkIn = checkInDate?.toStableDate(),
                            checkOut = checkOutDate?.toStableDate(),
                            onDateSelected = {
                                val date = it.localDate
                                val predictedCheckOutDate = checkInDate?.plusDays(includedNights)

                                if (predictedCheckOutDate != null && checkInDate != null && checkOutDate == null && date in checkInDate!!..predictedCheckOutDate.minusDays(1)) {
                                    return@Calendar
                                }

                                val updateCheckIn = when {
                                    checkInDate == null -> true
                                    checkOutDate != null -> true
                                    date.isBefore(checkInDate) -> true
                                    date == checkInDate!! -> true
                                    else -> false
                                }

                                if (updateCheckIn) {
                                    checkOutDate = null
                                    checkInDate = date
                                } else {
                                    checkOutDate = date
                                }
                            },
                            modifier = Modifier,
                            hasSameYearMonthAndDay = { year, month, day ->
                                val date = LocalDate.of(year, month, day)
                                val isCheckIn = checkInDate != null && checkInDate == date
                                val isCheckOut = checkOutDate != null && checkOutDate == date
                                val isInBetween = checkInDate != null && checkOutDate != null && date in checkInDate!!..checkOutDate!!
                                val isIncludedNight = checkInDate != null && date in checkInDate!!..checkInDate!!.plusDays(includedNights)

                                isCheckIn || isCheckOut || isInBetween || isIncludedNight
                            }
                        )
                    }
                }
            }
        }
    }

    private fun getIncludedNights(): Long {
        return Random.nextLong(1, 5)
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    CalendarComposeAndroidTheme {

    }
}